// Call it:
// node index.js path/to/file.shp path/to/file.dbf

var _         = require( 'lodash' ),
		fs        = require( 'fs' ),
		P         = require( 'bluebird' ),
		Shapefile = require( 'shapefile' ),
		leaseInfo = function ( initialize ) {
			_.defaults( this,
									_.pick( initialize.value.properties,
													[ 'LeaseId', 'State', 'Region', 'County', 'Acreage', 'Lessor1', 'LeaseHolde' ] ) );

		};

leaseInfo.prototype.toString = function () {
	return this.LeaseId + ':' + this.State + '-' + this.County + '-' + this.Region
				 + ' ' + this.Lessor1 + ' >>> ' + this.LeaseHolde;
};

Shapefile.open( process.argv[ 2 ], process.argv[ 3 ] )
				 .then( function ( src ) {
					 src.read()
							.then( function log( res ) {
								if ( res.done ) {
									return;
								}
								var l = new leaseInfo( res );
								console.log( l.toString() );
								return src.read().then( log )
							} )
				 } )
				 .catch( function ( err ) {
					 console.error( err.stack )
				 } );
